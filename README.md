<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZ2U2bWpkNnlpeDlnbHhrZWJ3NzF5bTRod3lhMGZndDhrazRzNHNoZCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/k7uYfGvphdTQNSIPzp/giphy.gif" alt="Xander Clemens" width="1000"/> <!-- Made my own gif -->


<h1 align="center"><a href="https://www.xanderclemens.com" target="blank"><img align="center" src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOHduZXMxNDExNGpvMGVmcmVuOHY5Z3F2ajd0YjhxN3JtdDExeWR5ZyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/QbWNwHwTmwBWwI7mwx/giphy.gif" alt="clemens_alex7" height="100" width="100" /></a> I'm Xander Clemens</h1>
<h2 style="text-align: center;">Full Stack Software Engineer</h2>

<p style="text-align: center;"><img src="https://komarev.com/ghpvc/?username=xanderrubio&label=Profile%20views&color=0e75b6&style=flat" alt="xanderrubio" /></p>

<p style="text-align: center;">
  <a href="https://www.linkedin.com/in/alexanderclemens" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white" alt="LinkedIn" height="25" width="90">
  </a>
  <a href="https://medium.com/@xanderclemens" target="_blank">
    <img alt="Medium" src="https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=yellow"  height="25" width="70" />
  </a>
  <a href="https://github.com/XanderRubio" target="_blank">
    <img alt="GitHub" src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=red" height="25" width="70" />
  </a>
</p>

<p style="text-align: center;">
  <img src="https://img.shields.io/badge/mac%20os-000000?style=for-the-badge&logo=apple&logoColor=whie" alt="Mac OS" height="24" width="70"/>
  <img src="https://img.shields.io/badge/Google_chrome-4285F4?style=for-the-badge&logo=Google-chrome&logoColor=white" alt="Google Chrome" height="21" width="90"/> 
</p>

- 🔭 I’m currently working on [HackChallenger🐍](https://gitlab.com/XanderRubio/HackChallenger) & [Before I Die Code](https://github.com/BeforeIDieCode/BeforeIDieAchievements)

- 🌱 I’m currently learning **Spanish🇨🇴(using Duolingo), Entrepreneurship and Generative AI**

  [![artysta's GitHub Statistics](https://artysta-cloud.vercel.app/api/duolingo/statistics?user=AlexanderC678040&renderTitle=false&fields=streak,totalXp,totalCrowns,learningLanguage)](https://github.com/artysta/artysta-cloud)

- 👨‍💻 All of my projects are available at [MY PORTFOLIO WEBSITE](https://www.xanderclemens.com/portfolio)

- 📝 I regularly write articles on [MEDIUM](https://xanderclemens.medium.com/)

- 💬 Ask me about **Python, Docker, React, Javascript, LLM's and Economics**

- 📫 How to reach me **clemens.alex7@gmail.com**

- 📄 Know about my experiences [RESUME](https://docs.google.com/document/d/1IyO5adOZSycdGaI0X8bTC9KA41hh-nVdYyGk_0WKT5Y/edit?usp=sharing)

- ⚡ Fun fact **In my free time, I dance Tango🕺**

---

<h3 align="left">Connect With Me:</h3>
<p align="left">
<a href="https://linkedin.com/in/alexanderclemens" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="alexanderclemens" height="50" width="50" /></a>
<a href="https://medium.com/@xanderclemens" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/medium.svg" alt="@xanderclemens" height="50" width="50" /></a>
<a href="https://www.youtube.com/channel/UCocfFCNHRhCT27RnHLPRcSw" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/youtube.svg" alt="ucocffcnhrhct27rnhlprcswu" height="50" width="50" /></a>
<a href="https://www.hackerrank.com/clemens_alex7" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/hackerrank.svg" alt="clemens_alex7" height="50" width="50" /></a>
<a href="https://www.xanderclemens.com" target="blank"><img align="center" src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOWxwYnNla2xwbDBveGd0azR6aHEzZDMyZnFvcHV0YzhudThmcG5ydiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/wjLcAEousXUKAYjoK3/giphy-downsized-large.gif" alt="Xander Clemens Website" height="50" width="50" /></a>
</p>

---

### Latest Medium ✍🏻 Post
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/0"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/0" alt="Most Recent Article">
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/1"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/1" alt="Recent Article 1">
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/2"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/2" alt="Recent Article 2">
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/3"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/3" alt="Recent Article 3">
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/4"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@xanderclemens/4" alt="Recent Article 4" >

---
<h3 align="left">🧰  My Toolbox:</h3>
<p align="left"> <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.djangoproject.com/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/django.svg" alt="django" width="40" height="40"/> </a> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> <a href="https://cloud.google.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://kubernetes.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kubernetes/kubernetes-icon.svg" alt="kubernetes" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> <a href="https://nodejs.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> </a> <a href="https://www.postgresql.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a> <a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://pytorch.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/pytorch/pytorch-icon.svg" alt="pytorch" width="40" height="40"/> </a> <a href="https://www.rabbitmq.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/rabbitmq/rabbitmq-icon.svg" alt="rabbitMQ" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://www.tensorflow.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/tensorflow/tensorflow-icon.svg" alt="tensorflow" width="40" height="40"/> </a> <a href="https://gitlab.com/XanderRubio" target="_blank" rel="noreferrer"> <img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExYjN5cG5vNWJmdHdkNzI0dHZ3ZGxvbGh2dzg3cnB5bTJ2ZHh2cDYweiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/4THbtyFjr0CFQhXNPU/giphy.gif" alt="GitLab" width="40" height="40"/> </a> </p>

---
<h3 align="center">🪚Supporting Documentation🧰</h3>
<h align="center">The following is a list of links that assisted in my building of this README.md special repository on GitHub and can easily be done.
</h>
<p></p>

- <a href="https://github.com/gautamkrishnar/blog-post-workflowgithub-readme-medium-recent-article">GitHub repository</a> - By <a href="https://github.com/gautamkrishnar">Gautam krishna R</a> that assisted in building out my workflows for displaying YoutTube and Blog posts

- <a href="https://github.com/bxcodec/github-readme-medium-recent-article">GitHub repository</a> - By <a href="https://github.com/bxcodec">Iman Tumorang</a> that assisted in helping to build the fetching ability for displaying Medium articles on the markdown file

- <a href="https://rahuldkjain.github.io/gh-profile-readme-generator/">GitHub repository</a> - By <a href="https://github.com/rahuldkjain">Rahul Jain</a> to assist with generating GitHub README generator

- <a href="https://www.youtube.com/watch?v=9A8sQZDRn5o&t=1s">YouTube Video</a> - By <a href="https://www.youtube.com/@fknight">
ForrestKnight</a> on "I Made a Custom GitHub Profile README Portfolio (and you can, too)"

- <a href="https://www.youtube.com/watch?v=ECuqb5Tv9qI&t=272s">YouTube Video</a> - By <a href="https://www.youtube.com/@codeSTACKr">codeSTACKr</a> on "Next Level GitHub Profile README (NEW) | How To Create An Amazing Profile ReadMe With GitHub Actions"

- <a href="https://www.youtube.com/watch?v=n6d4KHSKqGk">YouTube Video</a> - By <a href="https://www.youtube.com/@codeSTACKr">codeSTACKr</a> on "UPDATE: Next Level GitHub Profile README (NEW) | GitHub Actions | Vercel | Spotify"

- <a href="https://zzetao.github.io/awesome-github-profile/">Website</a> - Host 100s of awesome looking GitHub READMEs for inspiration. In addition, you can add your profile to this site and repository by <a href="https://github.com/abhisheknaiidu">Abhishek Naidu</a>

- <a href="https://github.com/badges/shields">GitHub repository</a> - By <a href="https://github.com/badges">Shields.io</a> that assisted with displaying custom display badges from numerous categories

- <a href="https://github.com/alexandresanlim/Badges4-README.md-Profile">GitHub repository</a> - To assist with displaying custom display badges from numerous categories by <a href="https://github.com/alexandresanlim">Alexandre Sanlim</a>

- <a href="https://github.com/artysta/duolingo">GitHub repository</a> -  By <a href="https://github.com/artysta">Adrian Kurek</a> that I used code to hack and display my Duolingo profile learning streak

- <a href="https://www.canva.com/">Canva</a> - Create custom imagery and save to create then GIFS that are under the memory limit when uploading

- <a href="https://giphy.com/ ">Giphy</a> - Used to create GIFS from images on <a href="https://www.canva.com/">Canva</a> to display
